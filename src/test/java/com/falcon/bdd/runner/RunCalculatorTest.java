package com.falcon.bdd.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		
        glue = "com.falcon.bdd.steps",
        features = {"src/test/resources/cucumber/calculator.feature", 
        		"src/test/resources/cucumber/google.feature",
        		"src/test/resources/cucumber/testrestapi.feature"
        }
)
public class RunCalculatorTest {
}
